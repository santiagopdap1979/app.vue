import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import HomeVue from './pages/HomeVue'
import LoginVue from './pages/LoginVue'
import PageError from './pages/PageError'
import ProductoPorId from './pages/ProductoPorId'
import CreateProducts from './pages/CreateProducts'


const routes = [

    {
        path:'/', component: HomeVue
    },
    {
        path:'/productoPorId/:numero', component: ProductoPorId, name: 'params'
    },
    {
        path:'/login', component: LoginVue
    },
    {
        path: '/crear', component: CreateProducts
    },
    {
        path: '/:pathMatch(.*)*', component: PageError
    }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});


const app = createApp(App).use(router);

app.use(router);

app.mount("#app");

